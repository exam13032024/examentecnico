package com.casa.examen.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Service;

@Service
public class Utilitario 
{
	
	  public String convertirAString(Date fecha) {
	        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
	        return formato.format(fecha);
	    }
	  
	  
	  public java.util.Date convertirADateSql(String fechaStr) throws ParseException {
	        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	        Date fecha = sdf.parse(fechaStr); // Convierte la cadena a java.util.Date
	        return fecha; // Convierte java.util.Date a java.sql.Date
	    }
	  

}
