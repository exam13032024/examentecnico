package com.casa.examen.dao.imp;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.casa.examen.controller.Respuesta;
import com.casa.examen.dao.ProductoDao;
import com.casa.examen.modelo.Producto;
import com.casa.examen.util.Utilitario;

import jakarta.persistence.EntityManager;
import jakarta.persistence.ParameterMode;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.StoredProcedureQuery;



@Service
@Transactional
public class ProductoDaoImpl  implements ProductoDao 
{

	@PersistenceContext
	EntityManager entityManager;
	
	@Autowired
	private Utilitario util;
	
	@Override
	public Respuesta agregar_verproducto( Producto producto ) 
	{
		StoredProcedureQuery query =null;
		List<Object[]> result = null;
		try {
			query = entityManager.createStoredProcedureQuery("sp_ejemplo")
			            .registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN)
			            .registerStoredProcedureParameter(2, String.class, ParameterMode.IN)
			            .registerStoredProcedureParameter(3, Date.class, ParameterMode.IN)
			            .registerStoredProcedureParameter(4, Class.class, ParameterMode.REF_CURSOR) // Ajustar si es necesario
			            .registerStoredProcedureParameter(5, Integer.class, ParameterMode.OUT)
			            .registerStoredProcedureParameter(6, String.class, ParameterMode.OUT)
			            .setParameter(1, producto.getPro_ide()   )
			            .setParameter(2, producto.getPro_nam()  )
			            .setParameter(3,   util.convertirADateSql(producto.getPro_fec()) );
		
		    query.execute();
		    result = query.getResultList();
	     	 
		
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		        
		        Integer codigo = (Integer) query.getOutputParameterValue(5);
		        String mensaje = (String) query.getOutputParameterValue(6);		        
		        System.out.println("Código: " + codigo + ", Mensaje: " + mensaje);
		        List<Producto> productos = new ArrayList<>();
		        
		        if( result !=null )
		        {
		        	  for (Object[] record : result) {
				        	BigDecimal prodId = (BigDecimal) record[0];
				            String prodNombre = (String) record[1];
				            java.util.Date prodFechaRegistro = (java.util.Date) record[2];
				            productos.add(new Producto(prodId.intValue(), prodNombre,  util.convertirAString(prodFechaRegistro) ));
				        }		
		        }
		        
		              
		
		      
		        
		return new Respuesta( codigo , mensaje,productos );
	}
	
	

}
