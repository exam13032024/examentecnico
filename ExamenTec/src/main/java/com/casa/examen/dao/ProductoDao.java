package com.casa.examen.dao;

import java.util.Date;
import java.util.List;

import com.casa.examen.controller.Respuesta;
import com.casa.examen.modelo.Producto;

public interface ProductoDao 
{
	
	Respuesta agregar_verproducto( Producto producto );

}
