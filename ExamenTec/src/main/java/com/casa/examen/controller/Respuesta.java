package com.casa.examen.controller;

import java.util.List;

import com.casa.examen.modelo.Producto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@ToString @EqualsAndHashCode
@Data
@RequiredArgsConstructor
public class Respuesta 
{  @NonNull
	private Integer codigo;
    @NonNull
	private String mensaje;
    @NonNull
	private List<Producto> lstProductos;
}
