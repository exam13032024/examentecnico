package com.casa.examen.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.casa.examen.dao.ProductoDao;
import com.casa.examen.modelo.Producto;
import com.casa.examen.util.Utilitario;

import jakarta.validation.Valid;


@RestController
@Validated // Asegúrate de que la clase está marcada para validación
public class CtProducto {
	
	@Autowired
	private ProductoDao productoDao;
	@Autowired
	private Utilitario util;
		
	@RequestMapping(value = "api/ctProducto", method = RequestMethod.POST)
    public ResponseEntity<Respuesta>  obtUsuario(@Valid  @RequestBody Producto  producto ) 
	{ 	return ResponseEntity.ok(productoDao.agregar_verproducto(producto)) ;
    }
	
	
	
}
