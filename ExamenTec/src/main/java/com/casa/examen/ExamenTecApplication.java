package com.casa.examen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamenTecApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamenTecApplication.class, args);
	}

}
