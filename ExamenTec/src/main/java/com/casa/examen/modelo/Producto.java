package com.casa.examen.modelo;



import java.sql.Date;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;

import lombok.RequiredArgsConstructor;
import lombok.ToString;

@ToString @EqualsAndHashCode
@Data
public class Producto {
	 
	 @NotNull(message = "El id no puede ser nulo")
	 @Positive(message = "El id debe ser positivo")
	private int pro_ide;
	@NotBlank(message = "El nombre no puede estar vacío")
	@Size(max = 100, message = "El nombre no puede tener más de 100 caracteres")
	private String pro_nam;
	@NotBlank(message = "La Fecha no puede estar vacío")
	private String pro_fec;

	public Producto( int pro_ide ,  String pro_nam , String pro_fec)
	{
		this.pro_ide= pro_ide;
		this.pro_nam= pro_nam;
		this.pro_fec= pro_fec;
	}
	 
	 
}
